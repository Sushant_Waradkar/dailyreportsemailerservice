﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using LearningMate.DailyReportsEmailerService.Interface;

namespace LearningMate.DailyReportsEmailerService.Utilities
{
    public class CommonFunctionsManager : ICommonFunctionsManager
    {
        /// <summary>
        /// This method Generates the Daily report and exports it to a Csv file within the configurable location provided.
        /// </summary>
        /// <param name="dsData"><see cref="DataSet"/>accepts DataSet with Reports generated from database</param>
        /// <param name="csvFile"><see cref="string"/>accepts csvFile path where the generated reports need to be exported</param>
        /// <returns></returns>
        public List<string> DataTableToCSV(DataSet dsData, string csvFile)
        {
            List<string> newList = new List<string>();

            foreach (DataTable DtValue in dsData.Tables)
            {
                StringBuilder sb = new StringBuilder();
                var columnNames = DtValue.Columns.Cast<DataColumn>().Select(column => "\"" + column.ColumnName.Replace("\"", "\"\"") + "\"").ToArray();
                sb.AppendLine(string.Join(",", columnNames));

                foreach (DataRow row in DtValue.Rows)
                {
                    var fields = row.ItemArray.Select(field => "\"" + field.ToString().Replace("\"", "\"\"") + "\"").ToArray();
                    sb.AppendLine(string.Join(",", fields));
                }
                csvFile = csvFile + DateTime.Now.ToString("MM-dd-yyyy");
                if (!Directory.Exists(csvFile))
                {
                    Directory.CreateDirectory(csvFile);
                    
                }
                //if (dsData.Tables.Count == 1 && dsData.Tables[0].Columns.Count==1)
                //{

                
                    csvFile = csvFile + Constant.ConsolidatedReport + DateTime.Now.ToString("MM-dd-yyyy") + ".csv";
                //}
                //else
                //{
                //    switch (DtValue.TableName)
                //    {
                //        case "Table":
                //            csvFile = csvFile + Constant.BulkUploadReport + DateTime.Now.ToString("MM-dd-yyyy") + ".csv";
                //            break;
                //        case "Table1":
                //            csvFile = csvFile + Constant.UserLoginReport + DateTime.Now.ToString("MM-dd-yyyy") + ".csv";
                //            break;
                //    }
                //}
               
                File.WriteAllText(csvFile, sb.ToString(), Encoding.Default);
                // passing csvReports as List
                newList.Add(csvFile);

            }
            return newList;
   
        }

       }
}
