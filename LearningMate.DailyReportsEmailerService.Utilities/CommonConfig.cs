﻿using System.Configuration;

namespace LearningMate.DailyReportsEmailerService.Utilities
{
    public class EmailConfig
    {

       
        public string ConfigAddress { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Recepient { get; set; }
        public string Subject { get; set; }
        /// <summary>
        /// Constructor that accepts ReportType and gets the Config Values for Email purpose
        /// </summary>
        /// <param name="Type"></param>
        public EmailConfig(int Type)
        {
            ConfigAddress = ConfigurationManager.AppSettings["FromMail"].ToString();
            UserName = ConfigurationManager.AppSettings["SMTPUserName"].ToString();
            Password = ConfigurationManager.AppSettings["SMTPUserPassword"].ToString();
            Recepient = ConfigurationManager.AppSettings["ToMail"].ToString();

            switch (Type)
            {
                case 1:
                    Subject = "NoNewFile";

                    break;
                default:
                    Subject = "Error";
                    break;


            }


        }
     }
    public  class Constant
    {

        public const string BulkUploadReport = @"\BulkUploadReport_";
        public const string UserLoginReport = @"\UserLoginReport_" ;
        public const string ConsolidatedReport = @"\ConsolidatedReport_";

    }

    public static class ReportProperties
    {
        public static string ReportType = ConfigurationManager.AppSettings["ReportType"].ToString();
    }

    
}
