﻿using LearningMate.DailyReportsEmailerService.Interface;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net;
using System.Net.Mail;

namespace LearningMate.DailyReportsEmailerService.Utilities.NotificationTypes
{
    public class Emailer : INotificationManager
    {
        /// <summary>
        /// This method uses the Email configurations and sends a daily report via mail to 
        /// </summary>
        /// <param name="Type"></param>
        /// <param name="attachmentPaths"></param>
        /// <returns></returns>
        public bool SendNotifcation_1(int Type, List<string> attachmentPaths = null)
        {
            EmailConfig objEmail = new EmailConfig(Type);
            MailMessage mail = new MailMessage(objEmail.ConfigAddress, objEmail.Recepient, objEmail.Subject, "");
            mail.IsBodyHtml = true;

            if (!string.IsNullOrEmpty(""))
            {
                MailAddress _cc = new MailAddress("");
                mail.CC.Add(_cc);
            }


            if (attachmentPaths != null)
            {
                foreach (var path in attachmentPaths)
                {
                    mail.Attachments.Add(new System.Net.Mail.Attachment(path));
                }

            }

            SmtpClient oClientMail = new SmtpClient(ConfigurationManager.AppSettings["SMTPServer"].ToString());
            oClientMail.UseDefaultCredentials = false;
            if (!String.IsNullOrEmpty(objEmail.UserName) && !String.IsNullOrEmpty(objEmail.Password))
            {
                oClientMail.Port = 587;
                oClientMail.EnableSsl = true;
                oClientMail.Credentials = new NetworkCredential(objEmail.UserName, objEmail.Password);
            }

            try
            {
                oClientMail.Send(mail);
            }
            catch (Exception ex)
            {
                string errorMessage = ex.Message;

                return false;
            }

            return true;
        }


        public void SendNotifcation()
        {
            throw new NotImplementedException();
        }
    }
}
