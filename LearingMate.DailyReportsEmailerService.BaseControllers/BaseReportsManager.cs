﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LearningMate.DailyReportsEmailerService.Interface;

namespace LearningMate.DailyReportsEmailerService.BaseControllers
{
    public class BaseReportsManager
    {
        IReportsManager _IReportsManager;
        
        public BaseReportsManager(IReportsManager IReportsManager_)
        {
            _IReportsManager = IReportsManager_;
        }

        public bool SendDailyReports()
        {
            return _IReportsManager.GenerateAndSendDailyReports();
        }


    }
}
