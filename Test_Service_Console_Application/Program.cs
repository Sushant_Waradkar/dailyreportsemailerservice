﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LearningMate.DailyReportsEmailerService.BusinessRules;
using LearningMate.DailyReportsEmailerService.Interface;
using LearningMate.DailyReportsEmailerService.BaseControllers;
using LearningMate.DailyReportsEmailerService.Utilities;
using LearningMate.DailyReportsEmailerService.DBInteractions;
using Microsoft.Practices.Unity;


namespace Test_Service_Console_Application
{
    class Program
    {
        static void Main(string[] args)
        {
            //IReportsManager IR;
            //IR = new ReportsManager();
            //IR.MailDailyReports();

            UnityContainer _UnityContainer = new UnityContainer();
            _UnityContainer.RegisterType(typeof(IReportsManager), typeof(ReportsManager));
            _UnityContainer.RegisterType(typeof(INotificationManager), typeof(NotificationManager));
            _UnityContainer.RegisterType(typeof(ICommonFunctionsManager), typeof(CommonFunctionsManager));
            _UnityContainer.RegisterType(typeof(IDataBaseInteractions), typeof(DBInteractions));
            var objReportsManager = _UnityContainer.Resolve<BaseReportsManager>();
            bool t = objReportsManager.SendDailyReports();
        }
    }
}
