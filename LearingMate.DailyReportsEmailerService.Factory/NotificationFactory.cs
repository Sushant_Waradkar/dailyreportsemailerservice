﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LearningMate.DailyReportsEmailerService.Interface;
using LearningMate.DailyReportsEmailerService.Utilities.NotificationTypes;

namespace LearingMate.DailyReportsEmailerService.Factory
{
    public class NotificationFactory
    {

        public static INotificationManager GetNotificationMAnagerInstance(int RequiredInstaceID)
        {
            INotificationManager _INotificationManager = null;
            switch(RequiredInstaceID)
            {
                case 0:
                    _INotificationManager =  new Emailer();
                    break;
                case 1:
                      _INotificationManager =  new WinowsEventLogs();
                    break;
                default:
                    break;
            }

            return _INotificationManager;
        }
    }
}
