﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LearningMate.DailyReportsEmailerService.Interface
{
    public interface ICommonFunctionsManager
    {
        List<string> DataTableToCSV(DataSet dsData, string csvFile);
    }
}
