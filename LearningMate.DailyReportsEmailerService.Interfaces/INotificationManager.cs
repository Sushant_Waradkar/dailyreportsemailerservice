﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LearningMate.DailyReportsEmailerService.Interface
{
    public interface INotificationManager
    {
         bool SendNotifcation_1(int iReportType, List<string> lstattachments);

         void SendNotifcation();
    }
}
