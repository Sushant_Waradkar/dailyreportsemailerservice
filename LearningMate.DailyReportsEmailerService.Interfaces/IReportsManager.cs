﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LearningMate.DailyReportsEmailerService.Interface
{
    public interface IReportsManager
    {
        bool GenerateAndSendDailyReports();
    }
}
