﻿using Scheduler.BusinessRules;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;


namespace Scheduler
{
    public partial class Service1 : ServiceBase
    {
        System.Timers.Timer _timer;
        DateTime _scheduleTime;
       
        public Service1()
        {
            System.Diagnostics.Debugger.Launch();
            InitializeComponent();
            _timer = new System.Timers.Timer();
            _scheduleTime = DateTime.Today.AddDays(1).AddHours(12);

            
        }

        protected override void OnStart(string[] args)
        {
            _timer.Enabled = true;
            _timer.Interval = _scheduleTime.Subtract(DateTime.Now).TotalSeconds * 1000;
            _timer.Elapsed += new System.Timers.ElapsedEventHandler(Timer_Elapsed);
        }

        protected void Timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
               
            // ----------------------------------

              BulkReports.MailDailyReports();
            // 2. If tick for the first time, reset next run to every 24 hours
            if (_timer.Interval != 24 * 60 * 60 * 1000)
            {
                _timer.Interval = 24 * 60 * 60 * 1000;
            }
        }


        private void WriteToFile(string sText)
        {
            string path = "D:\\ServiceLog1.txt";
            using (FileStream fs = new FileStream(path, FileMode.Append, FileAccess.Write))
            using (StreamWriter sw = new StreamWriter(fs))
            {
                sw.WriteLine(sText + "-" + DateTime.Now);
            }
        }

        protected override void OnStop()
        {
        }
    }
}
