﻿using LearningMate.DailyReportsEmailerService.BusinessRules;
using System;
using System.IO;
using System.ServiceProcess;
using Microsoft.Practices.Unity;
using LearningMate.DailyReportsEmailerService.Interface;
using LearningMate.DailyReportsEmailerService.Utilities;
using LearningMate.DailyReportsEmailerService.BaseControllers;
using LearningMate.DailyReportsEmailerService.DBInteractions;
using System.Configuration;


namespace LearningMate.DailyReportsEmailerService.MainService
{
    public partial class DailyReportsEmailerService : ServiceBase
    {
        System.Timers.Timer _timer;
        DateTime _scheduleTime;
        
        public DailyReportsEmailerService()
        {
         
          
            InitializeComponent();
            _timer = new System.Timers.Timer();
        
            _scheduleTime = DateTime.UtcNow.Date.AddDays(1).AddHours(Convert.ToDouble(ConfigurationManager.AppSettings["ScheduledTime"])).AddMinutes(Convert.ToDouble(ConfigurationManager.AppSettings["ScheduledTimeInMins"]));
        
            
        }
        /// <summary>
        /// This gets executed as soon as service starts. It initiates the timer that is set for service to call itself everyday once at the scheduled time.
        /// </summary>
        /// <param name="args"></param>
        protected override void OnStart(string[] args)
        {
            
            _timer.Enabled = true;
            _timer.Interval = _scheduleTime.Subtract(DateTime.UtcNow).TotalSeconds * 1000;
            _timer.Elapsed += new System.Timers.ElapsedEventHandler(Timer_Elapsed);
        }

        protected void Timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {

            //Main Method that sends reports at the scheduled Time.
             MailDailyReports();
            
            // 2. If tick for the first time, reset next run to every 24 hours
            if (_timer.Interval != 24 * 60 * 60 * 1000)
            {
                _timer.Interval = 24 * 60 * 60 * 1000;
            }
        }

        /// <summary>
        /// This method uses the dependency Injection and resolves all the dependencies.
        /// </summary>
        protected void MailDailyReports()
        {
             UnityContainer _UnityContainer = new UnityContainer();
            _UnityContainer.RegisterType(typeof(IReportsManager), typeof(ReportsManager));
            _UnityContainer.RegisterType(typeof(INotificationManager), typeof(NotificationManager));
            _UnityContainer.RegisterType(typeof(ICommonFunctionsManager), typeof(CommonFunctionsManager));
            _UnityContainer.RegisterType(typeof(IDataBaseInteractions), typeof(DBInteractions.DBInteractions));
            var objReportsManager = _UnityContainer.Resolve<BaseReportsManager>();
            bool t = objReportsManager.SendDailyReports();

        }
      
        
       
        
        protected override void OnStop()
        {
        }
    }
}
