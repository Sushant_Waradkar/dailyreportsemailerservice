﻿using LearingMate.DailyReportsEmailerService.Factory;
using LearningMate.DailyReportsEmailerService.Interface;
using System;
using System.Collections.Generic;

namespace LearningMate.DailyReportsEmailerService.Utilities
{
   public class NotificationManager : INotificationManager
    {
       /// <summary>
       /// This method sends the notification i.e Reports via email to the client
       /// </summary>
       /// <param name="iReportType">Accepts ReportType as parameter</param>
       /// <param name="lstattachments"></param>
       /// <returns></returns>
       public bool SendNotifcation_1(int iReportType, List<string> lstattachments)
       {
           int NotificationType = 0;
           INotificationManager objEmailer = null;
           objEmailer = NotificationFactory.GetNotificationMAnagerInstance(0);
           return objEmailer.SendNotifcation_1(iReportType, lstattachments);
       }

       public void SendNotifcation()
       {
           throw new NotImplementedException();
       }
    }
}
