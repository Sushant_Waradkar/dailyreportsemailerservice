﻿using LearningMate.DailyReportsEmailerService.Interface;
using LearningMate.DailyReportsEmailerService.Utilities;
using System.Collections.Generic;
using System.Configuration;
using System.Data;



namespace LearningMate.DailyReportsEmailerService.BusinessRules
{
    public class ReportsManager : IReportsManager
    {
        INotificationManager _NotificationManager;
        IDataBaseInteractions _DataBaseInteractions;
        ICommonFunctionsManager _CommonFunctionsManager;

        public ReportsManager(INotificationManager NotificationManager_, IDataBaseInteractions DataBaseInteractions_, ICommonFunctionsManager CommonFunctionsManager_)
        {
            _NotificationManager = NotificationManager_;
            _DataBaseInteractions = DataBaseInteractions_;
            _CommonFunctionsManager = CommonFunctionsManager_;
        }

        /// <summary>
        /// This method calls the DataTableToCsv method that genrerates the Reports and exports to Csv files. 
        /// The generated csv files are then send as attachments to SendNotification Method that mails the report to the client.
        /// </summary>
        /// <returns></returns>
        public bool GenerateAndSendDailyReports()
        {   
            DataSet ReportsDataSet = _DataBaseInteractions.GetReportsData();
            List<string> lstattachments = _CommonFunctionsManager.DataTableToCSV(ReportsDataSet, ConfigurationManager.AppSettings["RosterPath"].ToString());
            int iReportType = 0;
            if (_NotificationManager.SendNotifcation_1(iReportType, lstattachments))
                return true;
            else
                return false;
            
        }

    }
}
    